<!DOCTYPE html>
<html>
<head>
<title>Form</title>
</head>
<body>
    <h1>BUAT Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First name :</label><br><br>
        <input type="text" name="namadepan"><br><br>
        <label>Last name :</label><br><br>
        <input type="text" name="namabelakang"><br><br>
        <label>Gender :</label><br><br>
            <input type="radio" kelamin="Gender" value= "male"> Male <br>
            <input type="radio" kelamin="Gender" value= "female"> Female <br>
            <input type="radio" kelamin="Gender" value= "other"> Other <br>
        <br><br>
        <label>Nationality :</label><br><br>
        <select name ="Nationality" > 
            <option>Indonesia</option>
            <option>Malaysia</option>
            <option>Singapore</option>
            <option>Thailand</option>
        </select>
        <br><br>
        <label>Language Spoken :</label><br><br>
            <input type="checkbox" > Bahasa Indonesia <br>
            <input type="checkbox"> English <br>
            <input type="checkbox"> Other <br><br>
        <label> Bio :</label><br> <br>
        <textarea cols="30" rows="10"></textarea><br>
        <input type="submit" value="Kirim">
    </form>
</body>
</html>
