@extends('layout.master')

@section('Judul')
Halaman Tambah Cast
@endsection

@section('content') 
  
<form action="/cast" method="Post">
    @csrf
  <div class="form-group">
    <label>Nama Cast</label>
    <input type="text" name="nama" class="form-control" >
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="Integer" name="umur" class="form-control">
  </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" class="form-control"  cols="30" rows="10"></textarea>
  </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection