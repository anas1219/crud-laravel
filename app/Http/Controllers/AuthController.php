<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('halaman.form');
    }
    
    public function kirim(Request $request){
        $name1 = $request['namadepan'];
        $name2 = $request['namabelakang'];
        return view('halaman.welcome', compact('name1', 'name2'));
    }
}
